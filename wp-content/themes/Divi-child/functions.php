<?php
/**
 * Enqueue scripts and styles.
 */
function default_theme_scripts() {
	wp_enqueue_style( 'divi-child-main-css', get_stylesheet_directory_uri() . '/css/main.css');
	wp_enqueue_script( 'divi-child-main-js', get_stylesheet_directory_uri() . '/js/main.js', array(), '2017004', true );

}
add_action( 'wp_enqueue_scripts', 'default_theme_scripts' );
